/*
 * =====================================================================================
 *
 *       Filename:  linkedlist_tests.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  12/20/2014 20:17:00
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <lib/dbg.h>
#include "minunit.h"
#include <lib/linkedlist.h>

typedef struct IntListNode {
    LinkedListNode * next;
    int number;
} IntListNode;


void IntList_clear(LinkedList * linked_list)
{
    if(linked_list)
    {
        LIST_FOREACH_TWO(linked_list, head, next, cur)
        {
            if(cur)
            {
                free((IntListNode *)cur);
            }
        }
        free((IntListNode *)cur);
    }
}

IntListNode * IntListNode_create()
{
    IntListNode * new_node = calloc(1, sizeof(IntListNode));
    check_mem(new_node);

    new_node->next = NULL;
    new_node->number = 0;

    return new_node;
error:
    return NULL;
}

char * test_node_create()
{
    LinkedListNode * new_node = LinkedListNode_create();
    mu_assert(new_node != NULL, "Failed to create new node.");
    free(new_node);

    return NULL;
}

char * test_list_create()
{
    LinkedList * new_list = LinkedList_create();
    mu_assert(new_list != NULL, "Failed to create new list.");
    mu_assert(LinkedList_get_length(new_list) == 0, "Wrong length.");
    LinkedList_destroy(new_list);

    return NULL;
}

char * test_list_append()
{
    int number = 100;
    int i = 0;
    LinkedList * new_list = LinkedList_create();
    for(i=0; i<number; i++)
    {
        IntListNode * new_node = IntListNode_create();
        new_node->number = i;
        LinkedList_append(new_list, (LinkedListNode *)new_node);
    }
    mu_assert(LinkedList_get_length(new_list) == number, "Wrong length.");

    i = 0;
    LIST_FOREACH(new_list, head, next, cur)
    {
//        printf("%d\n", ((IntListNode *)cur)->number);
        mu_assert(i == ((IntListNode *)cur)->number, "Wrong number.");
        i ++;
    }

    LinkedList_destroy(new_list);

    return NULL;
}

char * test_list_insert()
{
    int i = 0;
    int number = 100;

    LinkedList * new_list = LinkedList_create();
    // try to insert into a empty list
    int rc = LinkedList_insert(new_list, NULL, NULL);
    mu_assert(-1 == rc, "Wrong return value.");

    // try to insert to the end
    IntListNode * new_node = IntListNode_create();
    new_node->number = -1;
    LinkedList_append(new_list, (LinkedListNode *)new_node);
    for(i=0; i<number; i++)
    {
        IntListNode * new_node = IntListNode_create();
        new_node->number = i;
        LinkedList_insert(new_list, new_list->tail, (LinkedListNode *)new_node);
    }
    mu_assert(LinkedList_get_length(new_list) == number+1, "Wrong length.");
    i = -1;
    LIST_FOREACH(new_list, head, next, cur)
    {
//        printf("%d\n", ((IntListNode *)cur)->number);
        mu_assert(((IntListNode *)cur)->number == i, "Wrong value.");
        i++;
    }
    

    LinkedList_destroy(new_list);

    // try to insert in the middle
    LinkedList * new_list2 = LinkedList_create();

    IntListNode * list[number];

    for(i=0; i<number; i++)
    {
        IntListNode * new_node = IntListNode_create();
        new_node->number = i;
        list[i] = new_node;
        LinkedList_append(new_list2, (LinkedListNode *)new_node);
    }

//    printf("%d\n", ((IntListNode *)new_list->head)->number);

    for(i=0; i<number; i++)
    {
        IntListNode * new_node = IntListNode_create();
        new_node->number = i;
        LinkedList_insert(new_list2, (LinkedListNode *)list[i], (LinkedListNode *)new_node);
        mu_assert(i+number+1 == LinkedList_get_length(new_list2), "Wrong length.");
//        printf("%d\n", LinkedList_get_length(new_list2));
    }

    i = 0;
    int j = 0;
    for(cur = _node = new_list2->head; _node != NULL; cur = _node = _node->next)
    {
//        printf("%d\n", ((IntListNode *)cur)->number);
        mu_assert(i == ((IntListNode *)cur)->number, "Wrong value.");
        if(j==1)
        {
            i ++;
            j = 0;
        }
        else if(j==0)
        {
            j = 1;
        }
        
    }


    LinkedList_destroy(new_list2);

    return NULL;
}

char * all_tests()
{
    mu_suite_start();

    mu_run_test(test_node_create);
    mu_run_test(test_list_create);
    mu_run_test(test_list_append);
    mu_run_test(test_list_insert);
    return NULL;
}

RUN_TESTS(all_tests);
