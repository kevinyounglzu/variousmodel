/*
 * =====================================================================================
 *
 *       Filename:  grow_tests.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  12/21/2014 16:00:12
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <lib/grow.h>
#include "minunit.h" 

char * test_cell_create()
{
    double time = 1.0;
    int flag = 1;

    Cell * new_cell = Cell_create(time, flag);
    mu_assert(NULL != new_cell, "Failed to create new cell.");
    mu_assert(Cell_get_time(new_cell) == time, "Wrong time");
    mu_assert(Cell_is_marked(new_cell) == flag, "Wrong status");

    free(new_cell);

    return NULL;
}

char * test_exponential()
{
    int i = 0;
    for(i=0; i<100; i++)
    {
        printf("%f\n", exponential(i * 0.01));
    }
    return NULL;
}

char * all_tests()
{
    mu_suite_start();

    mu_run_test(test_cell_create);
//    mu_run_test(test_exponential);

    return NULL;
}

RUN_TESTS(all_tests);
