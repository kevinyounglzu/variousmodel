#!/bin/bash
echo "All:"
git ls-files | xargs cat | wc -l

echo "c:"
git ls-files | grep -E "\.c|\.h" | xargs cat | wc -l

echo "python:"

git ls-files | grep -E "\.py" | xargs cat | wc -l
