# -*- coding: utf8 -*-
import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    data = np.loadtxt("result.txt")
    plt.plot(data.T[0], data.T[1])
    plt.show()
