# -*- coding: utf8 -*-
import numpy as np
import matplotlib.pyplot as plt
import itertools

if __name__ == "__main__":
    rwd = np.loadtxt("rwd.txt")
    dgrow = np.loadtxt("dgrow.txt")
    plt.hist(rwd.flatten(), bins=20, normed=True)
    plt.hist(dgrow.flatten(), bins=20, normed=True)
    plt.show()
