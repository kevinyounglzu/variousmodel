# -*- coding: utf8 -*-
import simulation
import math
import random
from Queue import PriorityQueue


class StochasticSimu(simulation.Simulation):
    def __init__(self, length, left_boundry, right_boundry):
        simulation.Simulation.__init__(self, length, left_boundry, right_boundry)
        self.queue = PriorityQueue()

        for cell in self.the_list:
            self.queue.put((self.nextTime(), cell))

    def run(self, time_limit):
        while self.current_time < time_limit:
            self.current_time, cell = self.queue.get()
            new_cell = simulation.Cell(0)
            self.the_list.insert(cell, new_cell)
            self.queue.put((self.current_time + self.nextTime(), cell))
            self.queue.put((self.current_time + self.nextTime(), new_cell))

    def nextTime(self):
        return -math.log(1.0 - random.random()) / 0.69


if __name__ == "__main__":
    times = 1000
    time = 1
    length = 24
    left_boundry = 12
    right_boundry = 18

    with open("sto.txt", "w+") as f:
        for i in range(times):
            sim = StochasticSimu(length, left_boundry, right_boundry)
            sim.run(time)
            for j in sim.returnMarked():
                f.write("%d\n" % j)
