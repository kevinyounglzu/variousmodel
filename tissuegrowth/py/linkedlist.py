# -*- coding: utf8 -*-
import unittest


class Node(object):
    def __init__(self):
        self.next = None
        self.last = None


class LinkedList(object):
    def __init__(self):
        self.length = 0
        self.head = None
        self.tail = None
        self.p = self.head

    def append(self, new_node):
        if not self.head:
            self.head = new_node
            self.tail = new_node
        else:
            self.tail.next = new_node
            new_node.last = self.tail
            self.tail = self.tail.next
        self.length += 1
        self.p = self.head

    def unshift(self, new_node):
        if not self.head:
            self.head = new_node
            self.tail = new_node
        else:
            self.head.last = new_node
            new_node.next = self.head
            self.head = self.head.last
        self.length += 1
        self.p = self.head

    def insert(self, target_node, new_node):
        if target_node == self.head:
            self.unshift(new_node)
            return 0

        new_node.last = target_node.last
        target_node.last.next = new_node
        target_node.last = new_node
        new_node.next = target_node
        self.length += 1
        self.p = self.head
        return 0

    def __iter__(self):
        return self

    def next(self):
        if self.p:
            temp_p = self.p
            self.p = self.p.next
            return temp_p
        else:
            self.p = self.head
            raise StopIteration


class TestLinkedList(unittest.TestCase):
    def setUp(self):
        self.the_list = LinkedList()

    def test_init(self):
        self.assertIsNone(self.the_list.head)
        self.assertIsNone(self.the_list.tail)
        self.assertEqual(self.the_list.length, 0)

    def test_append(self):
        node_number = 100
        node_list = []
        for i in range(node_number):
            new_node = Node()
            node_list.append(new_node)
            self.the_list.append(new_node)
            self.assertEqual(self.the_list.length, i+1)

        for i, node in enumerate(self.the_list):
            self.assertEqual(node_list[i], node)

    def test_unshift(self):
        node_number = 100
        node_list = []
        for i in range(node_number):
            new_node = Node()
            node_list.append(new_node)
            self.the_list.unshift(new_node)
            self.assertEqual(self.the_list.length, i+1)

        for i, node in enumerate(self.the_list):
            self.assertEqual(node_list[node_number - i - 1], node)

    def test_insert(self):
        # insert into a one element list
        node_1 = Node()
        self.the_list.append(node_1)
        node_2 = Node()
        self.the_list.insert(node_1, node_2)
        self.assertEqual(node_2, self.the_list.head)
        self.assertEqual(node_1, self.the_list.tail)

        # insert into the middle of list
        node_3 = Node()
        self.the_list.insert(node_1, node_3)
        self.assertEqual(node_3, self.the_list.head.next)


if __name__ == "__main__":
    unittest.main()
