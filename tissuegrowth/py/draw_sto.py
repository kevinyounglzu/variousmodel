# -*- coding: utf8 -*-
import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    sto = np.loadtxt("sto.txt")
    plt.hist(sto, bins=30, alpha=0.5, normed=True)
    plt.show()
