# -*- coding: utf8 -*-
import simulation
import random


class DeterminatedSimu(simulation.Simulation):
    def __init__(self, length, left_boundry, right_boundry):
        simulation.Simulation.__init__(self, length, left_boundry, right_boundry)

    def run(self, times):
        for i in range(times):
            length = self.the_list.length
            #print length
            the_one = int(random.random() * length) + 1

            for j, cell in enumerate(self.the_list):
                if j + 1 == the_one:
                    new_cell = simulation.Cell(0)
                    self.the_list.insert(cell, new_cell)


class CellList(object):
    def __init__(self, length, left_boundry, right_boundry):
        self.cells = range(left_boundry, right_boundry + 1)
        self.length = length

    def returnMarked(self):
        return self.cells


class DeterminatedSimu2(CellList):
    def __init__(self, length, left_boundry, right_boundry):
        CellList.__init__(self, length, left_boundry, right_boundry)

    def run(self, times):
        for i in range(times):
            the_one = int(random.random() * self.length) + 1
            for j, cell in enumerate(self.cells):
                if cell >= the_one:
                    self.cells[j] += 1
            self.length += 1


class RandomWalk(CellList):
    def __init__(self, length, left_boundry, right_boundry):
        CellList.__init__(self, length, left_boundry, right_boundry)

    def run(self, times):
        for t in range(times):
            for i, cell in enumerate(self.cells):
                if random.random() < cell / float(self.length):
                    self.cells[i] += 1
            self.length += 1


if __name__ == "__main__":
    times = 10000
    length = 24
    left_boundry = 12
    right_boundry = 18
    time = 60

    print "Start Simulation1"
    with open("d1.txt", "w+") as f:
        for i in range(times):
            sim = DeterminatedSimu(length, left_boundry, right_boundry)
            sim.run(time)
            for j in sim.returnMarked():
                f.write("%d\n" % j)

    print "Start Simulation2"
    with open("d2.txt", "w+") as f:
        for i in range(times):
            sim = DeterminatedSimu2(length, left_boundry, right_boundry)
            sim.run(time)
            for cell in sim.returnMarked():
                f.write("%d\n" % cell)

    print "Start Random walk"
    with open("rdw.txt", "w+") as f:
        for i in range(times):
            sim = RandomWalk(length, left_boundry, right_boundry)
            sim.run(time)
            for cell in sim.returnMarked():
                f.write("%d\n" % cell)
