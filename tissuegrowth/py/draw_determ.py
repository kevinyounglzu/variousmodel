# -*- coding: utf8 -*-
import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    #rwd = np.loadtxt("rwd.txt")
    #dgrow = np.loadtxt("dgrow.txt")
    d1 = np.loadtxt("d1.txt")
    d2 = np.loadtxt("d2.txt")
    rdw = np.loadtxt("rdw.txt")
    #plt.hist(rwd, bins=50, alpha=0.5, normed=True)
    #plt.hist(dgrow, bins=50, alpha=0.5, normed=True)
    plt.hist(d1, bins=30, alpha=0.5, normed=True)
    plt.hist(d2, bins=30, alpha=0.5, normed=True)
    plt.hist(rdw, bins=30, alpha=0.5, normed=True)
    plt.show()
