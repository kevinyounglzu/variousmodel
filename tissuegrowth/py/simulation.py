# -*- coding: utf8 -*-
import linkedlist
import unittest


class Cell(linkedlist.Node):
    def __init__(self, marked):
        linkedlist.Node.__init__(self)
        self.marked = marked


class Simulation(object):
    def __init__(self, length, left_boundry, right_boundry):
        self.the_list = linkedlist.LinkedList()
        self.current_time = 0

        for i in range(length):
            if(i >= left_boundry - 1 and i <= right_boundry - 1):
                flag = 1
            else:
                flag = 0
            self.the_list.append(Cell(flag))

    def returnMarked(self):
        marked_list = []
        for i, node in enumerate(self.the_list):
            if node.marked:
                marked_list.append(i + 1)
        return marked_list


class TestSimu(unittest.TestCase):
    def test_setup(self):
        length = 24
        left_boundry = 12
        right_boundry = 18
        simu = Simulation(length, left_boundry, right_boundry)

        for i, node in enumerate(simu.the_list):
            if i + 1 < left_boundry:
                self.assertEqual(node.marked, 0)
            elif i + 1 <= right_boundry:
                self.assertEqual(node.marked, 1)
            else:
                self.assertEqual(node.marked, 0)


if __name__ == "__main__":
    unittest.main()
