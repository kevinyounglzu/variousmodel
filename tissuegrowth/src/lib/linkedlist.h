/*
 * =====================================================================================
 *
 *       Filename:  linkedlist.h
 *
 *    Description:  An implementation of linked list
 *
 *        Version:  1.0
 *        Created:  12/20/2014 19:35:31
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef LinkedList_h
#define LinkedList_h

#include <lib/dbg.h>

typedef struct LinkedListNode {
    struct LinkedListNode * next;
} LinkedListNode;

typedef struct LinkedList {
    LinkedListNode * head;
    LinkedListNode * tail;
    int length;
} LinkedList;

LinkedListNode * LinkedListNode_create();

LinkedList * LinkedList_create();
void LinkedList_destroy(LinkedList * linked_list);

int LinkedList_insert(LinkedList * linked_list, LinkedListNode * target_node, LinkedListNode * new_node);
int LinkedList_append(LinkedList * linked_list, LinkedListNode * node);

#define LIST_FOREACH(L, H, N, C) LinkedListNode *_node = NULL;\
    LinkedListNode *C = NULL;\
    for(C = _node = L->H; _node != NULL; C = _node = _node->N)

#define LIST_FOREACH_TWO(L, H, N, C) LinkedListNode * C = NULL;\
    LinkedListNode * _next = NULL; \
    for(C = L->H, _next = C->N; _next != NULL; C = _next, _next = C->N)

#define LinkedList_get_length(A) ((A)->length)

#endif
