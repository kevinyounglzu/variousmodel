/*
 * =====================================================================================
 *
 *       Filename:  grow.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  12/20/2014 22:46:50
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef Grow_h
#define Grow_h

#include <lib/linkedlist.h>
#include <lib/dbg.h>

//typedef double (*)(double) Update_method;
//
//typedef struct Config {
//    double lambda;
//    Update_method judge;
//} Config;

typedef struct Cell {
    LinkedListNode * next;
    double time_last_update;
    int marked;
} Cell;

Cell * Cell_create(double time, int marked);

#define Cell_is_marked(A) ((A)->marked)
#define Cell_get_time(A) ((A)->time_last_update)

double exponential(double time);


#endif
