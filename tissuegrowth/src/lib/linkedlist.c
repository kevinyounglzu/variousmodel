/*
 * =====================================================================================
 *
 *       Filename:  linkedlist.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  12/20/2014 19:35:59
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <lib/linkedlist.h>

LinkedListNode * LinkedListNode_create()
{
    LinkedListNode * new_node = calloc(1, sizeof(LinkedListNode));
    check_mem(new_node);
    new_node->next = NULL;

    return new_node;
error:
    return NULL;
}

LinkedList * LinkedList_create()
{
    LinkedList * new_list = calloc(1, sizeof(LinkedList));
    check_mem(new_list);

    new_list->head = NULL;
    new_list->tail = NULL;

    return new_list;
error:
    return NULL;
}

void LinkedList_destroy(LinkedList * linked_list)
{
    LinkedListNode * current;
    LinkedListNode * next;
    if(linked_list->head)
    {
        for(current = linked_list->head, next = current->next; next != NULL; current = next, next = current->next)
        {
            if(current)
            {
                free(current);
            }
        }
        free(current);
    }
    free(linked_list);
}

int LinkedList_append(LinkedList * linked_list, LinkedListNode * node)
{
    if(linked_list->head && linked_list->tail)
    {
        linked_list->tail->next = node;
        linked_list->tail = node;
    }
    else
    {
        linked_list->head = node;
        linked_list->tail = node;
    }

    linked_list->length++;

    return 1;
}

// insert new_node to the end of target_node
// make sure the target node is in linked_list
int LinkedList_insert(LinkedList * linked_list, LinkedListNode * target_node, LinkedListNode * new_node)
{
    if(NULL == linked_list->head)
    {
        log_err("Try to insert to an empty list.");
        return -1;
    }

    if(target_node == linked_list->tail)
    {
        LinkedList_append(linked_list, new_node);
        return 0;
    }

    new_node->next = target_node->next;
    target_node->next = new_node;
    linked_list->length++;

    return 0;

}
