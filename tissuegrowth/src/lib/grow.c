/*
 * =====================================================================================
 *
 *       Filename:  grow.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  12/20/2014 22:54:38
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <lib/grow.h>
#include <stdlib.h>
#include <math.h>


/* create a new Cell struct
 *
 * @param time: current time
 * @param marked: wehter the cell is marked, only 1 or 0
 * 
 * @return a Cell struct with the related value
 * 
 */
Cell * Cell_create(double time, int marked)
{
    Cell * new_cell = calloc(1, sizeof(Cell));
    check_mem(new_cell);
    new_cell->next = NULL;
    new_cell->time_last_update = time;
    new_cell->marked = marked;

    return new_cell;
error:
    return NULL;
}

double exponential(double time)
{
    if(time < 0)
    {
        return 0.;
    }

    return 1 - exp(- 0.69 * time);
}
