# Introduction

This is my implementation of the simulation from [this work](http://journals.aps.org/pre/abstract/10.1103/PhysRevE.88.032704).

# python scripts

All three models are implemented in Python. The related scripts are under the ./py directory.

## linkedlist.py

The module linkedlist.py has a implementation of linked list.

### class Node

The node class is only tracking the next and last node in the list. For other usage, you can write your own subclass of Node and add things you need to it.

### class LinkedList

The LinkedList class tracking the head and tail of the list.

The methods are:

- LinkedList.append(new_node) appends new_node to the end of the list.
- LinkedList.unshift(new_node) attachs new_node to he head of the list.
- LinkedList.insert(target_node, new_node) inserts new_node right in front of target_node. Please make sure that target_node is in the list, this method will not check itself for efficiency.
- LinkedList itself is a iterable which means you can use `for` key word to iterate through the list. Don't modify the list in for loops or something bad will happen.

## simulation.py

### class Cell

Cell is a subclass of linkedlist.Node with only an extra attribute Cell.marked to indicate whether the cell is marked.

### class Simulation

Simulation is a base class for other simulation.

- Simulation takes the length of the tissue, left and right boundry as arguments and initialize a linkedlist with the right configuration.

- Simulation.returnMarked will return the index of the marked cells.

## stochastic.py

Use

    python stochastic.py

to run the simulation and use

    python draw_sto.py

to visualize the result, should produce FIG 3 in the article.

### class StochcasticSimu

StochcasticSimu is the main simulation class for stochastically growing model.

Use the right arguments to initialize and use StochcasticSimu.run to do the simulation.

## determ.py

Use

    python determ.py

to run the simulation and use

    python draw_determ.py

to visualize the result,, should produce FIG 6 in the article.

### class DeterminatedSimu

DeterminatedSimu is the main simulation class for determinated growing model.

Use the right arguments to initialize and use DeterminatedSimu.run to do the simulation.

### class CellList

CellList is a base class for other simulation.

### class DeterminatedSimu2

DeterminatedSimu2 is the same as DeterminatedSimu, only the algorithm is defferent.

### class RandomWalk

RandomWalk do the simulation of random walk model. The model itself produce the same result as determinated growing model.
