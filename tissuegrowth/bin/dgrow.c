/*
 * =====================================================================================
 *
 *       Filename:  dgrow.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  12/23/2014 23:15:13
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <lib/dbg.h>
#include <stdlib.h>
#include <stdio.h>
#include <lib/linkedlist.h>
#include <time.h>

#define MAX_TIME_STEP 100
#define TIMES 10000

typedef struct Cell {
    LinkedListNode * next;
    int marked;
} Cell;

double generateRandom(void)
{
    return rand() / (double) RAND_MAX;
}

Cell * Cell_create(int marked)
{
    Cell * new_cell = calloc(1, sizeof(Cell));
    check_mem(new_cell);
    new_cell->next = NULL;
    new_cell->marked = marked;

    return new_cell;
error:
    return NULL;
}

/* setup the simulation
 *
 * @param L0: the original length of the tissue
 * @param marked_left: left boundry of the marked cells
 * @param marked_right: right boundry of the marked cells 
 * @param cell_config: Config struct pointer
 *
 * @return a list of cell with the wanted setup
 * */
LinkedList * Setup(int L0, int marked_left, int marked_right)
{

    int i = 0;
    int flag = 0;
    
    LinkedList * the_list = LinkedList_create();
    check_mem(the_list);

    for(i=0; i<L0; i++)
    {
        if(i>= marked_left && i <= marked_right)
            flag = 1;
        else
            flag = 0;

        Cell * new_cell = Cell_create(flag);
        LinkedList_append(the_list, (LinkedListNode *)new_cell);
    }

    return the_list;
error:
    return NULL;
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main (void)
{
    int curent_time = 0;
    int length = 24;
    int the_one = 0;
    int counter = 0;
    int k = 0;

    //seed the random number
    srand(time(NULL));
    generateRandom();
    generateRandom();

//    while()

    LinkedList * the_list = NULL; 


    for(k=0; k<TIMES; k++)
    {
        curent_time = 0;
        length = 24;
        the_list = Setup(length, 12, 18);

        while(curent_time < MAX_TIME_STEP)
        {
            length = LinkedList_get_length(the_list);

            the_one = generateRandom() * length + 1;
            counter = 0;
    //        printf("%d\n", the_one);
            LIST_FOREACH(the_list, head, next, cur)
            {
                counter++;
                if(counter == the_one)
                {
                    Cell * new_cell = Cell_create(0);
                    LinkedList_insert(the_list, cur, (LinkedListNode *)new_cell);
                    break;
                }
            }
            curent_time++;
        }

        
        // print out the result
        counter = 0;
        LIST_FOREACH(the_list, head, next, cur)
        {
            counter++;
            if(((Cell *)cur)->marked)
            {
                printf("%d ", counter);
            }
        }
        printf("\n");

        LinkedList_destroy(the_list);

    }
    return EXIT_SUCCESS;
}
/* ----------  end of function main  ---------- */
