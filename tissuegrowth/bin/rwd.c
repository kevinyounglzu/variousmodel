/*
 * =====================================================================================
 *
 *       Filename:  rwd.c
 *
 *    Description:  The random walk model with determistic length
 *
 *        Version:  1.0
 *        Created:  12/23/2014 21:54:19
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define MAX_TIME_STEP 100
#define TIMES 100000

double generateRandom(void)
{
    return rand() / (double) RAND_MAX;
}

int Setup(int * cells, int number_of_cells, int * curent_time, int * curent_length, int begin_of_marked_cells, int length)
{
    *curent_time = 0;
    *curent_length = length;

    int i = 0;

    for(i=0; i<number_of_cells; i++)
    {
        cells[i] = begin_of_marked_cells; 
        begin_of_marked_cells++;
    }
    return 0;
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
int main (void)
{
    int length = 24;
    int number_of_cells = 6;
    int begin_of_marked_cells = 12;
    int cells[number_of_cells];

    int current_time = 0;
    int curent_length = 0;


    int i = 0;
    int k = 0;

    //seed the random number
    srand(time(NULL));
    generateRandom();
    generateRandom();


//    for(i=0; i<number_of_cells; i++)
//    {
//        printf("%d\n", cells[i]);
//    }

    for(k=0; k<TIMES; k++)
    {
        Setup(cells, number_of_cells, &current_time, &curent_length, begin_of_marked_cells, length);


        while(current_time < MAX_TIME_STEP)
        {
            for(i=0; i<number_of_cells; i++)
            {
                if(generateRandom() < cells[i] / (double)curent_length)
                {
                    // reproduce
                    cells[i]++;
                }
            }
            current_time++;
            curent_length++;
        }

        for(i=0; i<number_of_cells; i++)
        {
            printf("%d\n", cells[i]);
        }

    }
//        printf("\n");
    return EXIT_SUCCESS;
}
/* ----------  end of function main  ---------- */
