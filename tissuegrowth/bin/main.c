/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  12/21/2014 15:49:13
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <time.h>
#include <lib/linkedlist.h>
#include <lib/grow.h>
#include <lib/dbg.h>

double generateRandom(void)
{
    return rand() / (double) RAND_MAX;
}

/* setup the simulation
 *
 * @param L0: the original length of the tissue
 * @param marked_left: left boundry of the marked cells
 * @param marked_right: right boundry of the marked cells 
 * @param cell_config: Config struct pointer
 *
 * @return a list of cell with the wanted setup
 * */
LinkedList * Setup(int L0, int marked_left, int marked_right)
{

    int i = 0;
    int flag = 0;
    
    LinkedList * the_list = LinkedList_create();
    check_mem(the_list);

    for(i=0; i<L0; i++)
    {
        if(i>= marked_left && i <= marked_right)
            flag = 1;
        else
            flag = 0;

        Cell * new_cell = Cell_create(0.0, flag);
        LinkedList_append(the_list, (LinkedListNode *)new_cell);
    }

    return the_list;
error:
    return NULL;
}

int main (int argc, char *argv[])
{
    double timelimit = 1;
    double delta_t = 0.05;
    double current_time = 0.;
    int i = 0;

    //seed the random number
    srand(time(NULL));
    generateRandom();
    generateRandom();

    LinkedList * the_list = Setup(24, 12, 18);

    Cell * current_cell;

    while(current_time < timelimit)
    {
        printf("%f %d\n", current_time, LinkedList_get_length(the_list));

        LinkedListNode * _node = NULL;
        LinkedListNode * cur = NULL;

        for(cur = _node = the_list->head; _node != NULL; cur = _node = _node->next)
        {
            current_cell = (Cell *)cur;
//            printf("%d ", Cell_is_marked(current_cell));
            printf("%f ", current_time - Cell_get_time(current_cell));
        }
        printf("\n");

        for(cur = _node = the_list->head; _node != NULL; cur = _node = _node->next)
        {
            current_cell = (Cell *)cur;
            if(generateRandom() < exponential(current_time - Cell_get_time(current_cell)))
            {
                // produce a new cell
                Cell * new_cell = Cell_create(current_time, 0);
                Cell_get_time(current_cell) = current_time;
                // add to the list
                LinkedList_insert(the_list, (LinkedListNode *)current_cell, (LinkedListNode *)new_cell);
            }
        }
        current_time += delta_t;
    }
    
    
    LinkedList_destroy(the_list);
    return EXIT_SUCCESS;
}
/* ----------  end of function main  ---------- */
